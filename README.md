# Oh-li-shit

懶人工具 for 迅付

### 目前支援功能
  - 查詢支付平台(search)
  - 建立新增支付平台語法(create)
  - 比對支付平台及產生語法(diff)
  - 同步正式站白名單(bindip)

## 開始

### 安裝
```
sh -c "$(curl -fsSL https://gitlab.com/rd5-trade/lazytool/raw/master/install)"
```
如果有需要安裝迅付全部專案,請使用以下語法。
若想要使用ssh方式clone專案,後面再加上`--ssh`,但還是推薦使用http方式,密碼將會自動記錄五分鐘,不需重複輸入。
```
sh -c "$(curl -fsSL https://gitlab.com/rd5-trade/lazytool/raw/master/install)"  "" -t
```

### 使用方式
待補上
